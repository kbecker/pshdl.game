/**
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 * 
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 */
package org.pshdl.game.logic;

import com.google.common.base.Objects;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pshdl.game.logic.GameLevel;
import org.pshdl.rest.models.ModuleInformation;

@SuppressWarnings("all")
public class LevelStimulusGenerator {
  public static String generateCode(final GameLevel lvl) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*******************************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* PSHDL is a library and (trans-)compiler for PSHDL input. It generates");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     output suitable for implementation or simulation of it.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     This program is free software: you can redistribute it and/or modify");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     it under the terms of the GNU General Public License as published by");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     the Free Software Foundation, either version 3 of the License, or");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     (at your option) any later version.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     This program is distributed in the hope that it will be useful,");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     but WITHOUT ANY WARRANTY; without even the implied warranty of");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     GNU General Public License for more details.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     You should have received a copy of the GNU General Public License");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     along with this program.  If not, see <http://www.gnu.org/licenses/>.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     This License does not grant permission to use the trade names, trademarks,");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     service marks, or product names of the Licensor, except as required for");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     reasonable and customary use in describing the origin of the Work.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* Contributors:");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     Karsten Becker - initial API and implementation");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("******************************************************************************/");
    _builder.newLine();
    _builder.append("package org.pshdl.game.logic.levels;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import org.pshdl.game.logic.GameLevel.Stimuli;");
    _builder.newLine();
    _builder.append("import org.pshdl.game.logic.GameLevel.StimulusPackage;");
    _builder.newLine();
    _builder.append("import org.pshdl.game.logic.GameRunData.StimulusResultPackage;");
    _builder.newLine();
    _builder.append("import org.pshdl.game.logic.*;");
    _builder.newLine();
    _builder.append("import org.pshdl.interpreter.*;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class ");
    _builder.append(lvl.id);
    _builder.append(" implements StimulusRunner {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public GameRunData run(IHDLInterpreterFactory factory) throws Exception {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("final GameLevel lvl = GameLevel.levelMap.get(\"");
    _builder.append(lvl.id, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("final StimulusResultPackage[] results=new StimulusResultPackage[lvl.stimPkg.length];");
    _builder.newLine();
    {
      int _length = lvl.stimPkg.length;
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _length, true);
      for(final Integer i : _doubleDotLessThan) {
        _builder.append("\t\t");
        _builder.append("try (IHDLInterpreter interpreter");
        _builder.append(i, "\t\t");
        _builder.append("=factory.newInstance()) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("results[");
        _builder.append(i, "\t\t\t");
        _builder.append("]=runStimulusPackage");
        _builder.append(i, "\t\t\t");
        _builder.append("(interpreter");
        _builder.append(i, "\t\t\t");
        _builder.append(", lvl.stimPkg[");
        _builder.append(i, "\t\t\t");
        _builder.append("]);");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.append("\t\t");
    _builder.append("return new GameRunData(lvl.id, results);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    {
      int _length_1 = lvl.stimPkg.length;
      ExclusiveRange _doubleDotLessThan_1 = new ExclusiveRange(0, _length_1, true);
      for(final Integer i_1 : _doubleDotLessThan_1) {
        _builder.append("\t");
        final Function1<ModuleInformation.Port, Boolean> _function = (ModuleInformation.Port it) -> {
          return Boolean.valueOf(Objects.equal(it.name, "$clk"));
        };
        CharSequence _generatedStimPackge = LevelStimulusGenerator.generatedStimPackge(lvl.stimPkg[(i_1).intValue()], lvl.id, (i_1).intValue(), IterableExtensions.<ModuleInformation.Port>exists(lvl.expectedPorts, _function));
        _builder.append(_generatedStimPackge, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static CharSequence generatedStimPackge(final GameLevel.StimulusPackage lvl, final String lvlId, final int idx, final boolean hasClock) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public StimulusResultPackage runStimulusPackage");
    _builder.append(idx);
    _builder.append("(IHDLInterpreter interpreter, StimulusPackage sp) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("int matchingScore=0;");
    _builder.newLine();
    {
      int _length = lvl.input.length;
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _length, true);
      for(final Integer i : _doubleDotLessThan) {
        _builder.append("\t");
        CharSequence _generateInit = LevelStimulusGenerator.generateInit(lvl.input[(i).intValue()], lvl, lvlId, null);
        _builder.append(_generateInit, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      int _length_1 = lvl.output.length;
      ExclusiveRange _doubleDotLessThan_1 = new ExclusiveRange(0, _length_1, true);
      for(final Integer i_1 : _doubleDotLessThan_1) {
        _builder.append("\t");
        CharSequence _generateInit_1 = LevelStimulusGenerator.generateInit(lvl.output[(i_1).intValue()], lvl, lvlId, i_1);
        _builder.append(_generateInit_1, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      if (hasClock) {
        _builder.append("\t");
        _builder.append("final int clkIdx = interpreter.getIndex(\"");
        _builder.append(lvlId, "\t");
        _builder.append(".clk\");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("final int rstIdx = interpreter.getIndex(\"");
        _builder.append(lvlId, "\t");
        _builder.append(".rst\");");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      ExclusiveRange _doubleDotLessThan_2 = new ExclusiveRange(0, lvl.runLength, true);
      for(final Integer step : _doubleDotLessThan_2) {
        {
          for(final GameLevel.Stimuli inp : lvl.input) {
            _builder.append("\t");
            _builder.append("interpreter.setInput(");
            String _name = LevelStimulusGenerator.getName(inp);
            _builder.append(_name, "\t");
            _builder.append("Idx, ");
            int _get = inp.values[(step).intValue()];
            _builder.append(_get, "\t");
            _builder.append(");");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if (hasClock) {
            {
              if (((step).intValue() == 0)) {
                _builder.append("\t");
                _builder.append("interpreter.setInput(rstIdx, 1);");
                _builder.newLine();
              }
            }
            {
              if (((step).intValue() == 1)) {
                _builder.append("\t");
                _builder.append("interpreter.setInput(rstIdx, 0);");
                _builder.newLine();
              }
            }
            _builder.append("\t");
            _builder.append("interpreter.setInput(clkIdx, 1);");
            _builder.newLine();
          }
        }
        _builder.append("\t");
        _builder.append("interpreter.run();");
        _builder.newLine();
        {
          for(final GameLevel.Stimuli inp_1 : lvl.output) {
            _builder.append("\t");
            String _name_1 = LevelStimulusGenerator.getName(inp_1);
            _builder.append(_name_1, "\t");
            _builder.append("Generated.values[");
            _builder.append(step, "\t");
            _builder.append("] = (int) interpreter.getOutputLong(");
            String _name_2 = LevelStimulusGenerator.getName(inp_1);
            _builder.append(_name_2, "\t");
            _builder.append("Idx);");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("if (");
            String _name_3 = LevelStimulusGenerator.getName(inp_1);
            _builder.append(_name_3, "\t");
            _builder.append("Generated.values[");
            _builder.append(step, "\t");
            _builder.append("]==");
            String _name_4 = LevelStimulusGenerator.getName(inp_1);
            _builder.append(_name_4, "\t");
            _builder.append("Expected[");
            _builder.append(step, "\t");
            _builder.append("])");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("matchingScore++;");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("else");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("matchingScore-=5;");
            _builder.newLine();
          }
        }
        {
          if (hasClock) {
            _builder.append("\t");
            _builder.append("interpreter.setInput(clkIdx, 0);");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("interpreter.run();");
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("\t");
    _builder.append("return new StimulusResultPackage(new Stimuli[] { ");
    {
      int _length_2 = lvl.output.length;
      ExclusiveRange _doubleDotLessThan_3 = new ExclusiveRange(0, _length_2, true);
      boolean _hasElements = false;
      for(final Integer i_2 : _doubleDotLessThan_3) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "\t");
        }
        String _name_5 = LevelStimulusGenerator.getName(lvl.output[(i_2).intValue()]);
        _builder.append(_name_5, "\t");
        _builder.append("Generated");
      }
    }
    _builder.append(" }, null, matchingScore);");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static String getName(final GameLevel.Stimuli inp) {
    String res = inp.field;
    boolean _startsWith = res.startsWith("$");
    if (_startsWith) {
      return res.substring(1);
    }
    return res;
  }
  
  public static CharSequence generateLevelClass() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*******************************************************************************");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* PSHDL is a library and (trans-)compiler for PSHDL input. It generates");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     output suitable for implementation or simulation of it.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     This program is free software: you can redistribute it and/or modify");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     it under the terms of the GNU General Public License as published by");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     the Free Software Foundation, either version 3 of the License, or");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     (at your option) any later version.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     This program is distributed in the hope that it will be useful,");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     but WITHOUT ANY WARRANTY; without even the implied warranty of");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     GNU General Public License for more details.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     You should have received a copy of the GNU General Public License");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     along with this program.  If not, see <http://www.gnu.org/licenses/>.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     This License does not grant permission to use the trade names, trademarks,");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     service marks, or product names of the Licensor, except as required for");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     reasonable and customary use in describing the origin of the Work.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* Contributors:");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*     Karsten Becker - initial API and implementation");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("******************************************************************************/");
    _builder.newLine();
    _builder.append("package org.pshdl.game.logic.levels;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import java.util.*;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import org.pshdl.game.logic.*;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import com.google.common.collect.*;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class LevelStimulusFiles {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public final static Map<String, StimulusRunner> RUNNERS = initRunners();");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private static Map<String, StimulusRunner> initRunners() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("final Map<String, StimulusRunner> res = Maps.newHashMap();");
    _builder.newLine();
    {
      for(final GameLevel lvl : GameLevel.LEVELS) {
        _builder.append("\t\t");
        _builder.append("res.put(\"");
        _builder.append(lvl.id, "\t\t");
        _builder.append("\", new ");
        _builder.append(lvl.id, "\t\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("return res;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateInit(final GameLevel.Stimuli inp, final GameLevel.StimulusPackage lvl, final String lvlId, final Integer idx) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("final int ");
    String _name = LevelStimulusGenerator.getName(inp);
    _builder.append(_name);
    _builder.append("Idx = interpreter.getIndex(\"");
    _builder.append(lvlId);
    _builder.append(".");
    String _name_1 = LevelStimulusGenerator.getName(inp);
    _builder.append(_name_1);
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    {
      if ((idx != null)) {
        _builder.append("final Stimuli ");
        String _name_2 = LevelStimulusGenerator.getName(inp);
        _builder.append(_name_2);
        _builder.append("Generated = new Stimuli();");
        _builder.newLineIfNotEmpty();
        String _name_3 = LevelStimulusGenerator.getName(inp);
        _builder.append(_name_3);
        _builder.append("Generated.values = new int[sp.runLength];");
        _builder.newLineIfNotEmpty();
        String _name_4 = LevelStimulusGenerator.getName(inp);
        _builder.append(_name_4);
        _builder.append("Generated.field = sp.output[");
        _builder.append(idx);
        _builder.append("].field;");
        _builder.newLineIfNotEmpty();
        _builder.append("int[] ");
        String _name_5 = LevelStimulusGenerator.getName(inp);
        _builder.append(_name_5);
        _builder.append("Expected = sp.output[");
        _builder.append(idx);
        _builder.append("].values;");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
}
