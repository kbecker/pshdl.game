/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.jersey;

import javax.ws.rs.WebApplicationException;

public class WorkspaceFileException extends WebApplicationException {

    /**
     *
     */
    private static final long serialVersionUID = 6272243769837321176L;
    public final String wid;
    public final String file;

    public WorkspaceFileException(Exception e, String wid, String file) {
        super(e);
        this.wid = wid;
        this.file = file;
    }

    @Override
    public String getMessage() {
        final StringBuilder sb = new StringBuilder();
        if (wid != null) {
            sb.append("Workspace:").append(wid).append(' ');
        }
        if (file != null) {
            sb.append("File:").append(file).append(' ');
        }
        if (getCause() instanceof WebApplicationException) {
            final WebApplicationException webapp = (WebApplicationException) getCause();
            sb.append(webapp.getResponse().getEntity());
        } else {
            sb.append(getCause().getMessage());
        }
        return sb.toString();
    }
}
