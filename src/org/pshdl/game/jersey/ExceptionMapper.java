/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.jersey;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.google.common.base.Throwables;

@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Throwable> {
    Logger log = Logger.getLogger(ExceptionMapper.class.getName());

    @Override
    public Response toResponse(Throwable ex) {
        if (ex instanceof NotFoundException) {
            return Response.status(404).build();
        }
        if (ex instanceof WebApplicationException) {
            final WebApplicationException wae = (WebApplicationException) ex;
            final Response response = wae.getResponse();
            String errors;
            if (ex instanceof WorkspaceFileException) {
                final WorkspaceFileException wfe = (WorkspaceFileException) ex;
                errors = Throwables.getStackTraceAsString(wfe.getCause());
            } else {
                errors = Throwables.getStackTraceAsString(ex);
            }
            final String[] lines = errors.split("\n");
            final StringBuilder sb = new StringBuilder();
            sb.append(lines[0]).append('\n');
            for (int i = 1; i < lines.length; i++) {
                final String l = lines[i];
                if (l.startsWith("\tat org.pshdl.")) {
                    sb.append(l).append('\n');
                } else {
                    break;
                }
            }
            log.log(Level.WARNING, "Exception occured: " + response.getStatus() + " " + response.getEntity(), ex);
            return Response.status(response.getStatus()).entity(sb.toString()).type(MediaType.TEXT_PLAIN).build();
        }
        log.log(Level.WARNING, "Exception occured", ex);
        final StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return Response.serverError().entity("Internal server error:\n" + errors.toString()).type(MediaType.TEXT_PLAIN).build();
    }

}
