/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.jersey;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.sse.SseFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class RestApp extends ResourceConfig {

    public static class CORSFilter implements ContainerResponseFilter {

        @Override
        public void filter(ContainerRequestContext req, ContainerResponseContext response) throws IOException {
            // instead of "*" you should provide a specific domain name for
            // security reasons
            final MultivaluedMap<String, Object> httpHeaders = response.getHeaders();

            if (req.getHeaderString("Origin") != null) {
                httpHeaders.add("Access-Control-Allow-Origin", req.getHeaderString("Origin"));
                httpHeaders.add("Access-Control-Expose-Headers", "X-Cache-Date");
            }

            if ("OPTIONS".equals(req.getMethod())) {
                httpHeaders.add("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
                httpHeaders.add("Access-Control-Allow-Headers", "Origin, Content-Type, X-Cache-Date");
                httpHeaders.add("Access-Control-Max-Age", "-1");
            }
        }
    }

    public RestApp() {
        registerClasses(Waver.class);
        register(MultiPartFeature.class);
        register(CORSFilter.class);
        register(SseFeature.class);
        register(ExceptionMapper.class);
        // register(LoggingFilter.class);

    }
}
