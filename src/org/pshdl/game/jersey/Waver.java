/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.jersey;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.pshdl.game.logic.GameLevel;
import org.pshdl.game.logic.GameRunData;
import org.pshdl.game.logic.StimulusRunner;
import org.pshdl.game.logic.levels.LevelStimulusFiles;
import org.pshdl.interpreter.ExecutableModel;
import org.pshdl.interpreter.FastSimpleInterpreter.FastSimpleFactory;
import org.pshdl.interpreter.costs.SimpleEstimator;
import org.pshdl.interpreter.costs.SimpleEstimator.ResourceCosts;
import org.pshdl.interpreter.utils.Graph.Cycle;
import org.pshdl.interpreter.utils.Graph.CycleException;
import org.pshdl.model.HDLArithOp;
import org.pshdl.model.HDLArithOp.HDLArithOpType;
import org.pshdl.model.HDLInterface;
import org.pshdl.model.HDLPackage;
import org.pshdl.model.HDLPrimitive;
import org.pshdl.model.HDLUnit;
import org.pshdl.model.HDLVariable;
import org.pshdl.model.HDLVariableDeclaration;
import org.pshdl.model.HDLVariableDeclaration.HDLDirection;
import org.pshdl.model.evaluation.HDLEvaluationContext;
import org.pshdl.model.simulation.PStoEXCompiler;
import org.pshdl.model.types.builtIn.HDLBuiltInAnnotationProvider.HDLBuiltInAnnotations;
import org.pshdl.model.types.builtIn.HDLPrimitives;
import org.pshdl.model.utils.HDLQuery;
import org.pshdl.model.utils.PSAbstractCompiler;
import org.pshdl.model.utils.services.IHDLValidator.IErrorCode;
import org.pshdl.model.validation.Problem;
import org.pshdl.model.validation.Problem.ProblemSeverity;
import org.pshdl.rest.models.CompileInfo;
import org.pshdl.rest.models.FileRecord;
import org.pshdl.rest.models.ModuleInformation.Port;
import org.pshdl.rest.models.ProblemInfo;
import org.pshdl.workspace.JSONHelper;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

@Path("/")
public class Waver {
    private static final Logger LOG = Logger.getLogger(Waver.class.getName());
    private static final ObjectWriter writer = JSONHelper.getWriter();

    enum GameLevelValidation implements IErrorCode {
        module_not_found(ProblemSeverity.ERROR), port_not_found(ProblemSeverity.ERROR), expected_port_not_primitive(ProblemSeverity.ERROR), width_not_constant(
                ProblemSeverity.ERROR), width_not_correct(
                        ProblemSeverity.ERROR), unstimulated_in_port(ProblemSeverity.WARNING), no_default_clock(ProblemSeverity.ERROR), mod_not_supported(ProblemSeverity.ERROR);

        GameLevelValidation(ProblemSeverity severity) {
            this.severity = severity;
        }

        private final ProblemSeverity severity;

        @Override
        public ProblemSeverity getSeverity() {
            return severity;
        }
    }

    @POST
    @Path("/{wid}/run/{level}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response POST_run(@PathParam("wid") String wid, @PathParam("level") String level) {
        final GameLevel lvl = GameLevel.levelMap.get(level);
        final String fileName = level + ".pshdl";
        if (lvl == null) {
            throw new WorkspaceFileException(new IllegalArgumentException("No such level:" + level), wid, fileName);
        }
        final PSAbstractCompiler psab = new PSAbstractCompiler();
        try {
            final File workspacePath = getWorkspacePath(wid);
            final File inputFile = new File(workspacePath, fileName);
            psab.add(inputFile, null);
            final Optional<HDLUnit> unit = psab.getUnits().stream().filter((HDLUnit u) -> u.getName().equals(level)).findAny();
            if (unit.isPresent()) {
                final HDLUnit hdlUnit = unit.get();
                final ExecutableModel executable = PStoEXCompiler.createExecutable(hdlUnit, fileName, false, false);
                final SimpleEstimator estimator = new SimpleEstimator();
                final Map<String, ResourceCosts> estimateFrame = estimator.estimateFrameCosts(executable, new SimpleEstimator.MaxAreaCostSelector());
                final StimulusRunner runner = LevelStimulusFiles.RUNNERS.get(lvl.id);
                final GameRunData run = runner.run(new FastSimpleFactory(executable, false, false));
                run.hwCosts = estimateFrame.values().stream().mapToInt((a) -> a.area).sum();
                return Response.ok().entity(writer.writeValueAsBytes(run)).build();
            }
            return Response.serverError().entity("Did not find the expected module. Did you run validation before?").build();
        } catch (final CycleException ce) {
            return Response.status(409).entity(ce.model.humanReadableExplaination((Cycle<String, ?>) ce.cycle)).build();
        } catch (final Exception e) {
            e.printStackTrace();
            throw new WorkspaceFileException(e, wid, fileName);
        } finally {
            psab.close();
        }
    }

    @POST
    @Path("/{wid}/validate/{level}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response POST_validate(@PathParam("wid") String wid, @PathParam("level") String level) {
        final GameLevel lvl = GameLevel.levelMap.get(level);
        final String fileName = level + ".pshdl";
        if (lvl == null) {
            throw new WorkspaceFileException(new IllegalArgumentException("No such level:" + level), wid, fileName);
        }
        final PSAbstractCompiler psab = new PSAbstractCompiler();
        try {
            final File workspacePath = getWorkspacePath(wid);
            final File inputFile = new File(workspacePath, fileName);
            psab.add(inputFile, null);
            psab.validatePackages();
            final Map<String, Set<Problem>> allProblems = psab.getAllProblems();
            final Set<Problem> set = allProblems.getOrDefault(inputFile.getAbsolutePath(), Sets.newHashSet());
            final Optional<HDLUnit> unit = psab.getUnits().stream().filter((HDLUnit u) -> u.getName().equals(level)).findAny();
            if (unit.isPresent()) {
                final HDLUnit hdlUnit = unit.get();
                checkHDLUnit(lvl, set, hdlUnit);
            } else {
                final HDLPackage hdlPackage = psab.getFileUnits().get(inputFile.getAbsolutePath());
                if (hdlPackage != null) {
                    set.add(new Problem(GameLevelValidation.module_not_found, hdlPackage, "Expected a module named '" + lvl.id + "' but did not find any"));
                }
            }
            final Stream<ProblemInfo> map = set.stream().map((p) -> {
                final ProblemInfo pi = new ProblemInfo();
                pi.setFromProblem(p);
                return pi;
            });
            final CompileInfo ci = new CompileInfo();
            ci.setProblems(map.collect(Collectors.toList()));
            ci.setFiles(Lists.newArrayList(new FileRecord(inputFile, workspacePath, wid)));
            return Response.ok().entity(writer.writeValueAsString(ci)).build();
        } catch (final Exception e) {
            e.printStackTrace();
            throw new WorkspaceFileException(e, wid, fileName);
        } finally {
            psab.close();
        }
    }

    public void checkHDLUnit(final GameLevel lvl, final Set<Problem> set, final HDLUnit hdlUnit) {
        final HDLInterface asInterface = hdlUnit.asInterface(null);
        final Map<String, Port> expectedFields = Maps.newLinkedHashMap();
        for (final Port port : lvl.expectedPorts) {
            expectedFields.put(port.name, port);
        }
        final HDLEvaluationContext context = HDLEvaluationContext.createDefault(asInterface);
        asInterface.getPorts().forEach((port) -> {
            for (final HDLVariable var : port.getVariables()) {
                checkVariable(set, expectedFields, context, var, hdlUnit);
            }
        });
        for (final Port stim : expectedFields.values()) {
            final String expectedPort = "Expected to find a port '" + getPort(stim) + "'";
            set.add(new Problem(GameLevelValidation.port_not_found, hdlUnit, expectedPort));
        }
        final Collection<HDLArithOp> all = HDLQuery.select(HDLArithOp.class).from(hdlUnit).where(HDLArithOp.fType).isEqualTo(HDLArithOpType.MOD).getAll();
        for (final HDLArithOp op : all) {
            set.add(new Problem(GameLevelValidation.mod_not_supported, op, "The modulo operation is not supported in the simulation"));
        }
    }

    public void checkVariable(final Set<Problem> set, final Map<String, Port> expectedFields, final HDLEvaluationContext context, final HDLVariable var, HDLUnit unit) {
        String name = var.getName();
        if (var.getAnnotation(HDLBuiltInAnnotations.clock) != null) {
            name = "$clk";
        }
        if (var.getAnnotation(HDLBuiltInAnnotations.reset) != null) {
            name = "$rst";
        }
        final Port stimuli = expectedFields.remove(name);
        if (stimuli != null) {
            final String expectedPort = "Expected to find a port '" + getPort(stimuli) + "'";
            final HDLPrimitive primitive = var.getContainer(HDLVariableDeclaration.class).getPrimitive();
            if (primitive == null) {
                set.add(new Problem(GameLevelValidation.expected_port_not_primitive, var, expectedPort));
            } else {
                final Integer width = HDLPrimitives.getWidth(primitive, context);
                if (width == null) {
                    set.add(new Problem(GameLevelValidation.width_not_constant, var, expectedPort));
                } else {
                    if (width != stimuli.width) {
                        set.add(new Problem(GameLevelValidation.width_not_correct, var, expectedPort));
                    }
                }
            }
        } else {
            if ((var.getDirection() == HDLDirection.IN) || (var.getDirection() == HDLDirection.INOUT)) {
                if (var.getName().equals("clk")) {
                    set.add(new Problem(GameLevelValidation.no_default_clock, unit,
                            "There is no default clock ($clk) in this level, so you can't use <b>register</b> without specifiyng which signal should be used as clock: register(clock=xyz)"));
                } else {
                    set.add(new Problem(GameLevelValidation.unstimulated_in_port, var, "This in port will not have any input"));
                }
            }
        }
    }

    private String getPort(Port stim) {
        final StringBuilder sb = new StringBuilder();
        sb.append(stim.dir).append(' ');
        sb.append(stim.primitive);
        switch (stim.primitive) {
        case BIT:
        case BOOL:
        case INTEGER:
        case NATURAL:
        case STRING:
            break;
        case BITVECTOR:
        case INT:
        case UINT:
            sb.append('<').append(stim.width).append('>');
            break;
        default:
            break;
        }
        sb.append(' ').append(stim.name);
        return sb.toString();
    }

    @GET
    @Path("levels")
    @Produces(MediaType.APPLICATION_JSON)
    public Response simulate() {
        return Response.ok().entity(GameLevel.class.getResourceAsStream("levels.json")).build();
    }

    public static final File BASEDIR = new File("/var/pshdl");

    public static File getWorkspacePath(String wid) {
        final File file = new File(BASEDIR, wid);
        try {
            final String fc = file.getCanonicalPath();
            final String bc = BASEDIR.getCanonicalPath();
            if (!fc.startsWith(bc)) {
                LOG.log(Level.WARNING, "getWorkspacePath()" + wid);
                throw new IllegalArgumentException("The workspace ID is not valid");
            }
        } catch (final IOException e) {
            throw new IllegalArgumentException("The workspace ID is not valid");
        }
        return file;
    }
}
