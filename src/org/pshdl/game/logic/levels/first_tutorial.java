/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic.levels;

import org.pshdl.game.logic.GameLevel;
import org.pshdl.game.logic.GameLevel.Stimuli;
import org.pshdl.game.logic.GameLevel.StimulusPackage;
import org.pshdl.game.logic.GameRunData;
import org.pshdl.game.logic.GameRunData.StimulusResultPackage;
import org.pshdl.game.logic.StimulusRunner;
import org.pshdl.interpreter.IHDLInterpreter;
import org.pshdl.interpreter.IHDLInterpreterFactory;

public class first_tutorial implements StimulusRunner {

    @Override
    public GameRunData run(IHDLInterpreterFactory factory) throws Exception {
        final GameLevel lvl = GameLevel.levelMap.get("first_tutorial");
        final StimulusResultPackage[] results = new StimulusResultPackage[lvl.stimPkg.length];
        try (IHDLInterpreter interpreter0 = factory.newInstance()) {
            results[0] = runStimulusPackage0(interpreter0, lvl.stimPkg[0]);
        }
        return new GameRunData(lvl.id, results);
    }

    public StimulusResultPackage runStimulusPackage0(IHDLInterpreter interpreter, StimulusPackage sp) {
        int matchingScore = 0;
        final int aIdx = interpreter.getIndex("first_tutorial.a");
        final int bIdx = interpreter.getIndex("first_tutorial.b");
        final int cIdx = interpreter.getIndex("first_tutorial.c");
        final int dIdx = interpreter.getIndex("first_tutorial.d");
        final Stimuli dGenerated = new Stimuli();
        dGenerated.values = new int[sp.runLength];
        dGenerated.field = sp.output[0].field;
        final int[] dExpected = sp.output[0].values;
        final int eIdx = interpreter.getIndex("first_tutorial.e");
        final Stimuli eGenerated = new Stimuli();
        eGenerated.values = new int[sp.runLength];
        eGenerated.field = sp.output[1].field;
        final int[] eExpected = sp.output[1].values;
        interpreter.setInput(aIdx, 0);
        interpreter.setInput(bIdx, 0);
        interpreter.setInput(cIdx, 0);
        interpreter.run();
        dGenerated.values[0] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[0] == dExpected[0]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[0] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[0] == eExpected[0]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.setInput(bIdx, 0);
        interpreter.setInput(cIdx, 0);
        interpreter.run();
        dGenerated.values[1] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[1] == dExpected[1]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[1] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[1] == eExpected[1]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 0);
        interpreter.setInput(bIdx, 1);
        interpreter.setInput(cIdx, 0);
        interpreter.run();
        dGenerated.values[2] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[2] == dExpected[2]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[2] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[2] == eExpected[2]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.setInput(bIdx, 1);
        interpreter.setInput(cIdx, 0);
        interpreter.run();
        dGenerated.values[3] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[3] == dExpected[3]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[3] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[3] == eExpected[3]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 0);
        interpreter.setInput(bIdx, 0);
        interpreter.setInput(cIdx, 1);
        interpreter.run();
        dGenerated.values[4] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[4] == dExpected[4]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[4] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[4] == eExpected[4]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.setInput(bIdx, 0);
        interpreter.setInput(cIdx, 1);
        interpreter.run();
        dGenerated.values[5] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[5] == dExpected[5]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[5] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[5] == eExpected[5]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 0);
        interpreter.setInput(bIdx, 1);
        interpreter.setInput(cIdx, 1);
        interpreter.run();
        dGenerated.values[6] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[6] == dExpected[6]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[6] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[6] == eExpected[6]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.setInput(bIdx, 1);
        interpreter.setInput(cIdx, 1);
        interpreter.run();
        dGenerated.values[7] = (int) interpreter.getOutputLong(dIdx);
        if (dGenerated.values[7] == dExpected[7]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        eGenerated.values[7] = (int) interpreter.getOutputLong(eIdx);
        if (eGenerated.values[7] == eExpected[7]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        return new StimulusResultPackage(new Stimuli[] { dGenerated, eGenerated }, null, matchingScore);
    }
}
