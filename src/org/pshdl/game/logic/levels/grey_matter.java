/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic.levels;

import org.pshdl.game.logic.GameLevel;
import org.pshdl.game.logic.GameLevel.Stimuli;
import org.pshdl.game.logic.GameLevel.StimulusPackage;
import org.pshdl.game.logic.GameRunData;
import org.pshdl.game.logic.GameRunData.StimulusResultPackage;
import org.pshdl.game.logic.StimulusRunner;
import org.pshdl.interpreter.IHDLInterpreter;
import org.pshdl.interpreter.IHDLInterpreterFactory;

public class grey_matter implements StimulusRunner {

    @Override
    public GameRunData run(IHDLInterpreterFactory factory) throws Exception {
        final GameLevel lvl = GameLevel.levelMap.get("grey_matter");
        final StimulusResultPackage[] results = new StimulusResultPackage[lvl.stimPkg.length];
        try (IHDLInterpreter interpreter0 = factory.newInstance()) {
            results[0] = runStimulusPackage0(interpreter0, lvl.stimPkg[0]);
        }
        return new GameRunData(lvl.id, results);
    }

    public StimulusResultPackage runStimulusPackage0(IHDLInterpreter interpreter, StimulusPackage sp) {
        int matchingScore = 0;
        final int oIdx = interpreter.getIndex("grey_matter.o");
        final Stimuli oGenerated = new Stimuli();
        oGenerated.values = new int[sp.runLength];
        oGenerated.field = sp.output[0].field;
        final int[] oExpected = sp.output[0].values;
        final int clkIdx = interpreter.getIndex("grey_matter.clk");
        final int rstIdx = interpreter.getIndex("grey_matter.rst");
        interpreter.setInput(rstIdx, 1);
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[0] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[0] == oExpected[0]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(rstIdx, 0);
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[1] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[1] == oExpected[1]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[2] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[2] == oExpected[2]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[3] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[3] == oExpected[3]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[4] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[4] == oExpected[4]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[5] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[5] == oExpected[5]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[6] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[6] == oExpected[6]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[7] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[7] == oExpected[7]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[8] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[8] == oExpected[8]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[9] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[9] == oExpected[9]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[10] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[10] == oExpected[10]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[11] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[11] == oExpected[11]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[12] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[12] == oExpected[12]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[13] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[13] == oExpected[13]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[14] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[14] == oExpected[14]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[15] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[15] == oExpected[15]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[16] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[16] == oExpected[16]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[17] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[17] == oExpected[17]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        interpreter.setInput(clkIdx, 1);
        interpreter.run();
        oGenerated.values[18] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[18] == oExpected[18]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(clkIdx, 0);
        interpreter.run();
        return new StimulusResultPackage(new Stimuli[] { oGenerated }, null, matchingScore);
    }
}
