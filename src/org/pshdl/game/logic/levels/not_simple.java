/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic.levels;

import org.pshdl.game.logic.GameLevel;
import org.pshdl.game.logic.GameLevel.Stimuli;
import org.pshdl.game.logic.GameLevel.StimulusPackage;
import org.pshdl.game.logic.GameRunData;
import org.pshdl.game.logic.GameRunData.StimulusResultPackage;
import org.pshdl.game.logic.StimulusRunner;
import org.pshdl.interpreter.IHDLInterpreter;
import org.pshdl.interpreter.IHDLInterpreterFactory;

public class not_simple implements StimulusRunner {

    @Override
    public GameRunData run(IHDLInterpreterFactory factory) throws Exception {
        final GameLevel lvl = GameLevel.levelMap.get("not_simple");
        final StimulusResultPackage[] results = new StimulusResultPackage[lvl.stimPkg.length];
        try (IHDLInterpreter interpreter0 = factory.newInstance()) {
            results[0] = runStimulusPackage0(interpreter0, lvl.stimPkg[0]);
        }
        return new GameRunData(lvl.id, results);
    }

    public StimulusResultPackage runStimulusPackage0(IHDLInterpreter interpreter, StimulusPackage sp) {
        int matchingScore = 0;
        final int aIdx = interpreter.getIndex("not_simple.a");
        final int oIdx = interpreter.getIndex("not_simple.o");
        final Stimuli oGenerated = new Stimuli();
        oGenerated.values = new int[sp.runLength];
        oGenerated.field = sp.output[0].field;
        final int[] oExpected = sp.output[0].values;
        interpreter.setInput(aIdx, 0);
        interpreter.run();
        oGenerated.values[0] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[0] == oExpected[0]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.run();
        oGenerated.values[1] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[1] == oExpected[1]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.run();
        oGenerated.values[2] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[2] == oExpected[2]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.run();
        oGenerated.values[3] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[3] == oExpected[3]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 0);
        interpreter.run();
        oGenerated.values[4] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[4] == oExpected[4]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.run();
        oGenerated.values[5] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[5] == oExpected[5]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 0);
        interpreter.run();
        oGenerated.values[6] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[6] == oExpected[6]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 0);
        interpreter.run();
        oGenerated.values[7] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[7] == oExpected[7]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 1);
        interpreter.run();
        oGenerated.values[8] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[8] == oExpected[8]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        interpreter.setInput(aIdx, 0);
        interpreter.run();
        oGenerated.values[9] = (int) interpreter.getOutputLong(oIdx);
        if (oGenerated.values[9] == oExpected[9]) {
            matchingScore++;
        } else {
            matchingScore -= 5;
        }
        return new StimulusResultPackage(new Stimuli[] { oGenerated }, null, matchingScore);
    }
}
