/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic.levels;

import java.util.Map;

import org.pshdl.game.logic.StimulusRunner;

import com.google.common.collect.Maps;

public class LevelStimulusFiles {
    public final static Map<String, StimulusRunner> RUNNERS = initRunners();

    private static Map<String, StimulusRunner> initRunners() {
        final Map<String, StimulusRunner> res = Maps.newLinkedHashMap();
        res.put("not_simple", new not_simple());
        res.put("slow_me", new slow_me());
        res.put("first_tutorial", new first_tutorial());
        res.put("iq_test", new iq_test());
        res.put("comm_test", new comm_test());
        res.put("grey_matter", new grey_matter());
        return res;
    }
}
