/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic;

import java.util.Map;
import java.util.Optional;

import org.pshdl.game.logic.levels.LevelStimulusFiles;
import org.pshdl.interpreter.ExecutableModel;
import org.pshdl.interpreter.FastSimpleInterpreter;
import org.pshdl.interpreter.IHDLInterpreterFactory;
import org.pshdl.interpreter.costs.SimpleEstimator;
import org.pshdl.interpreter.costs.SimpleEstimator.ResourceCosts;
import org.pshdl.interpreter.utils.Graph.CycleException;
import org.pshdl.model.HDLPackage;
import org.pshdl.model.HDLUnit;
import org.pshdl.model.simulation.PStoEXCompiler;
import org.pshdl.model.utils.TestcaseReducer.CrashValidator.CrashValidatorRunnable;

public class GameCrasher implements CrashValidatorRunnable {

    private final GameLevel lvl;
    private final boolean filterCycle;
    public static int cycleCounter = 0;

    public GameCrasher(GameLevel lvl, boolean filterCycle) {
        this.lvl = lvl;
        this.filterCycle = filterCycle;
    }

    @Override
    public void run(String src, HDLPackage pkg) throws Throwable {
        final ExecutableModel executable = getExecutable(src, pkg);
        if (executable != null) {
            runGame(executable, new FastSimpleInterpreter.FastSimpleFactory(executable, false, false));
        }
    }

    public ExecutableModel getExecutable(String src, HDLPackage pkg) throws CycleException {
        final Optional<HDLUnit> optional = pkg.getUnits().stream().filter((HDLUnit u) -> u.getName().equals(lvl.id)).findAny();
        if (optional.isPresent()) {
            try {
                return PStoEXCompiler.createExecutable(optional.get(), src, false, true);
            } catch (final CycleException e) {
                cycleCounter++;
                if (!filterCycle) {
                    throw e;
                }
            }
        }
        return null;
    }

    public void runGame(final ExecutableModel executable, IHDLInterpreterFactory factory) throws Exception {
        final SimpleEstimator estimator = new SimpleEstimator();
        final Map<String, ResourceCosts> estimateFrame = estimator.estimateFrameCosts(executable, new SimpleEstimator.MaxAreaCostSelector());
        final StimulusRunner runner = LevelStimulusFiles.RUNNERS.get(lvl.id);
        final GameRunData run = runner.run(factory);
    }

}
