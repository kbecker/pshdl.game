/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.pshdl.game.logic.GameLevel.Stimuli;
import org.pshdl.game.logic.GameLevel.StimulusPackage;
import org.pshdl.rest.models.utils.DartCodeGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.io.Files;

public class GameRunData {

    public static class StimulusResultPackage {
        @JsonProperty
        public final Stimuli[] generated, userGenerated;
        @JsonProperty
        public final int matchingScore;

        public StimulusResultPackage() {
            generated = null;
            userGenerated = null;
            matchingScore = -1;
        }

        public StimulusResultPackage(Stimuli[] generated, Stimuli[] userGenerated, int matchingScore) {
            super();
            this.generated = generated;
            this.userGenerated = userGenerated;
            this.matchingScore = matchingScore;
        }

    }

    @JsonProperty
    public final String lvl;

    @JsonProperty
    public final StimulusResultPackage[] results;

    @JsonProperty
    public int hwCosts;

    public GameRunData() {
        lvl = null;
        results = null;
    }

    public GameRunData(String lvl, StimulusResultPackage[] results) {
        super();
        this.lvl = lvl;
        this.results = results;
    }

    public static void main(String[] args) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("import 'dart:convert';\n" + "import 'package:pshdl_api/pshdl_api.dart';\n" + "import 'package:observe/observe.dart';");
        sb.append(DartCodeGenerator.generateClass(Stimuli.class));
        sb.append(DartCodeGenerator.generateClass(StimulusPackage.class));
        sb.append(DartCodeGenerator.generateClass(GameLevel.class));
        sb.append(DartCodeGenerator.generateClass(StimulusResultPackage.class));
        sb.append(DartCodeGenerator.generateClass(GameRunData.class));

        Files.write(sb, new File("/Users/karstenbecker/GDrive/PSHDL_Game/web/game_json.dart"), StandardCharsets.UTF_8);
    }
}
