/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.pshdl.rest.models.ModuleInformation.Port;
import org.pshdl.workspace.JSONHelper;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.common.io.Files;

public class GameLevel {
    public static class StimulusPackage {
        @JsonProperty
        public Stimuli[] input, output;

        @JsonProperty
        public int runLength;

        public StimulusPackage() {
        }

        public StimulusPackage(Stimuli[] input, Stimuli[] output, int runLength) {
            super();
            this.input = input;
            this.output = output;
            this.runLength = runLength;
        }

        public Stimuli getInputStimulus(String name) {
            for (final Stimuli stim : input) {
                if (stim.field.equals(name)) {
                    return stim;
                }
            }
            return null;
        }

        public Stimuli getOutputStimulus(String name) {
            for (final Stimuli stim : output) {
                if (stim.field.equals(name)) {
                    return stim;
                }
            }
            return null;
        }
    }

    public static class Stimuli {
        @JsonProperty
        public String field;
        @JsonProperty
        public int[] values;

        public Stimuli() {
        }

    }

    @JsonProperty
    public List<Port> expectedPorts;

    @JsonProperty
    public StimulusPackage stimPkg[];
    @JsonProperty
    public String name, id, defaultCode;

    public GameLevel() {
    }

    public final static Map<String, GameLevel> levelMap = Maps.newLinkedHashMap();
    public final static GameLevel[] LEVELS = loadLevels();

    public static GameLevel[] loadLevels() {
        final ObjectReader reader = JSONHelper.getReader(GameLevel[].class);
        try {
            final GameLevel[] levels = reader.readValue(GameLevel.class.getResourceAsStream("/levels.json"));
            for (final GameLevel level : levels) {
                levelMap.put(level.id, level);
            }
            return levels;
        } catch (final IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws IOException {
        final File dir = new File("/Users/karstenbecker/Dropbox/PSHDL/org.pshdl.game/src/org/pshdl/game/logic/levels");
        for (final GameLevel lvl : LEVELS) {
            final String generateCode = LevelStimulusGenerator.generateCode(lvl);
            Files.write(generateCode, new File(dir, lvl.id + ".java"), Charsets.UTF_8);
        }
        Files.write(LevelStimulusGenerator.generateLevelClass(), new File(dir, "LevelStimulusFiles.java"), Charsets.UTF_8);
    }
}
