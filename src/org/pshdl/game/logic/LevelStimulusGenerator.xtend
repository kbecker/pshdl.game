/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic

import org.pshdl.game.logic.GameLevel.Stimuli
import org.pshdl.game.logic.GameLevel.StimulusPackage

class LevelStimulusGenerator {
	def static String generateCode(GameLevel lvl) '''/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic.levels;

import org.pshdl.game.logic.GameLevel.Stimuli;
import org.pshdl.game.logic.GameLevel.StimulusPackage;
import org.pshdl.game.logic.GameRunData.StimulusResultPackage;
import org.pshdl.game.logic.*;
import org.pshdl.interpreter.*;

public class «lvl.id» implements StimulusRunner {

	@Override
	public GameRunData run(IHDLInterpreterFactory factory) throws Exception {
		final GameLevel lvl = GameLevel.levelMap.get("«lvl.id»");
		final StimulusResultPackage[] results=new StimulusResultPackage[lvl.stimPkg.length];
		«FOR i:0..<lvl.stimPkg.length»
			try (IHDLInterpreter interpreter«i»=factory.newInstance()) {
				results[«i»]=runStimulusPackage«i»(interpreter«i», lvl.stimPkg[«i»]);
			}
		«ENDFOR»
		return new GameRunData(lvl.id, results);
	}

	«FOR i:0..<lvl.stimPkg.length»
		«generatedStimPackge(lvl.stimPkg.get(i), lvl.id, i, lvl.expectedPorts.exists[it.name=="$clk"])»
	«ENDFOR»
}

	'''
	
	def static generatedStimPackge(StimulusPackage lvl, String lvlId, int idx, boolean hasClock)'''
	public StimulusResultPackage runStimulusPackage«idx»(IHDLInterpreter interpreter, StimulusPackage sp) {
		int matchingScore=0;
		«FOR i : 0 ..< lvl.input.length»
		«generateInit(lvl.input.get(i), lvl, lvlId, null)»
		«ENDFOR»
		«FOR i : 0 ..< lvl.output.length»
		«generateInit(lvl.output.get(i), lvl, lvlId, i)»
		«ENDFOR»
		«IF hasClock»
			final int clkIdx = interpreter.getIndex("«lvlId».clk");
			final int rstIdx = interpreter.getIndex("«lvlId».rst");
		«ENDIF»
		«FOR step : 0 ..< lvl.runLength»
			«FOR inp : lvl.input»
			interpreter.setInput(«inp.name»Idx, «inp.values.get(step)»);
			«ENDFOR»
			«IF hasClock»
				«IF step==0»
					interpreter.setInput(rstIdx, 1);
				«ENDIF»
				«IF step==1»
					interpreter.setInput(rstIdx, 0);
				«ENDIF»
				interpreter.setInput(clkIdx, 1);
			«ENDIF»
			interpreter.run();
			«FOR inp : lvl.output»
			«inp.name»Generated.values[«step»] = (int) interpreter.getOutputLong(«inp.name»Idx);
			if («inp.name»Generated.values[«step»]==«inp.name»Expected[«step»])
				matchingScore++;
			else
				matchingScore-=5;
			«ENDFOR»
			«IF hasClock»
				interpreter.setInput(clkIdx, 0);
				interpreter.run();
			«ENDIF»
		«ENDFOR»
		return new StimulusResultPackage(new Stimuli[] { «FOR i : 0 ..< lvl.output.length SEPARATOR ','»«lvl.output.get(i).name»Generated«ENDFOR» }, null, matchingScore);
	}
	'''

	def static getName(Stimuli inp) {
		var res = inp.field
		if (res.startsWith('$'))
			return res.substring(1)
		return res;
	}

	def static generateLevelClass() '''/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game.logic.levels;

import java.util.*;

import org.pshdl.game.logic.*;

import com.google.common.collect.*;

public class LevelStimulusFiles {
	public final static Map<String, StimulusRunner> RUNNERS = initRunners();

	private static Map<String, StimulusRunner> initRunners() {
		final Map<String, StimulusRunner> res = Maps.newHashMap();
		«FOR lvl : GameLevel.LEVELS»
		res.put("«lvl.id»", new «lvl.id»());
		«ENDFOR»
		return res;
	}
}
	'''

	def static generateInit(Stimuli inp, StimulusPackage lvl, String lvlId, Integer idx) '''
		final int «inp.name»Idx = interpreter.getIndex("«lvlId».«inp.name»");
		«IF idx!==null»
			final Stimuli «inp.name»Generated = new Stimuli();
			«inp.name»Generated.values = new int[sp.runLength];
			«inp.name»Generated.field = sp.output[«idx»].field;
			int[] «inp.name»Expected = sp.output[«idx»].values;
		«ENDIF»
	'''
}
