/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.game;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.ProcessingException;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.pshdl.game.jersey.RestApp;
import org.pshdl.model.utils.HDLCore;

public class GameServer {
    public static void main(String[] args) throws ProcessingException, URISyntaxException, InterruptedException {
        final boolean testing = args.length != 0;
        final int port = Integer.parseInt(args[0]);
        HDLCore.defaultInit();
        final HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(new URI(getURL(testing, port)), new RestApp());
        if (testing) {
            final StaticHttpHandler dartOldHandler = new StaticHttpHandler("/Users/karstenbecker/GDrive/PSHDL_Game/build/web");
            dartOldHandler.setFileCacheEnabled(false);
            httpServer.getServerConfiguration().addHttpHandler(dartOldHandler, "/dart");
        }
        while (true) {
            Thread.sleep(10000);
        }
    }

    private static String getURL(boolean testing, int port) {
        final String portString = (testing ? port : 80) + "/";
        return !testing ? "http://87.230.27.37:" + portString : "http://0.0.0.0:" + portString;
    }
}
